#ifndef BitEditor_H
#define BitEditor_H

#include <QWidget>
#include "BitArray.h"
#include <QBoxLayout>
#include <QGridLayout>
#include <QCheckBox>
#include <QVector>
#include <QLabel>

class BitEditor : public QWidget
{
    Q_OBJECT
public:
    explicit BitEditor(BitArray *array, bool editable = true, QWidget *parent = 0);

signals:
    void valueChanged();
    
public slots:
    void updateView();

private slots:
    void cBoxChangedSlot();

private:

    void updateBoxes();

    QGridLayout *mGridLayout;
    BitArray *mArray;
    bool mEditable;
    QVector<QLabel*> mLabels;
    QVector<QCheckBox*> mBoxes;
};

#endif // BitEditor_H
