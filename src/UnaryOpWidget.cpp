#include "UnaryOpWidget.h"

UnaryOpWidget::UnaryOpWidget(BitArray *arr, QWidget *parent)
    : QWidget(parent), array(arr)
{
    QVBoxLayout *topLayout = new QVBoxLayout(this);
    QVBoxLayout *vLayout = new QVBoxLayout();
    QHBoxLayout *hLayout = new QHBoxLayout();
    QHBoxLayout *hLayout2 = new QHBoxLayout();
    pBtnShiftRight = new QPushButton(">>");
    pBtnShiftLeft = new QPushButton("<<");
    sBShiftVal = new QSpinBox();
    pBtnNegate = new QPushButton("~");
    pBtnSwap = new QPushButton("Swap");
    pBtnReverse = new QPushButton("Rev");
    chkBoxRot = new QCheckBox("Rot");

    QGroupBox *mGroupBox = new QGroupBox("Unary operations", this);
    QVBoxLayout *groupBoxLayout = new QVBoxLayout(mGroupBox);

    groupBoxLayout->addLayout(vLayout);
    groupBoxLayout->setContentsMargins(5, 0, 5, 5);
    topLayout->addWidget(mGroupBox);
    topLayout->setContentsMargins(0, 0, 0, 0);

    pBtnShiftRight->setToolTip("Shift/Rotate right");
    pBtnShiftLeft->setToolTip("Shift/Rotate left");
    chkBoxRot->setToolTip("Rotate");
    sBShiftVal->setToolTip("Shift value");
    pBtnNegate->setToolTip("Negate");
    pBtnSwap->setToolTip("Swap bytes");
    pBtnReverse->setToolTip("Reverse bits");

    sBShiftVal->setValue(1);

    pBtnShiftLeft->setFixedWidth(pBtnShiftLeft->sizeHint().height());
    pBtnShiftRight->setFixedWidth(pBtnShiftRight->sizeHint().height());
    sBShiftVal->setFixedWidth(pBtnShiftRight->sizeHint().height()*2);
    pBtnNegate->setFixedWidth(pBtnNegate->sizeHint().height()*2);
    pBtnSwap->setFixedWidth(pBtnSwap->sizeHint().height()*2);
    pBtnReverse->setFixedWidth(pBtnReverse->sizeHint().height()*2);

    hLayout->addWidget(pBtnShiftLeft);
    hLayout->addWidget(sBShiftVal);
    hLayout->addWidget(pBtnShiftRight);
    hLayout->addWidget(chkBoxRot);

    hLayout2->addWidget(pBtnNegate);
    hLayout2->addWidget(pBtnSwap);
    hLayout2->addWidget(pBtnReverse);

    vLayout->addLayout(hLayout);
    vLayout->addLayout(hLayout2);
    vLayout->insertStretch(-1);


    hLayout->setContentsMargins(0, 0, 0, 0);
    vLayout->setContentsMargins(0, 0, 0, 0);

    connect(pBtnShiftLeft, SIGNAL(clicked()), this, SLOT(shiftLeft()));
    connect(pBtnShiftRight, SIGNAL(clicked()), this, SLOT(shiftRight()));
    connect(pBtnNegate, SIGNAL(clicked()), this, SLOT(negate()));
    connect(pBtnSwap, SIGNAL(clicked()), this, SLOT(swap()));
    connect(pBtnReverse, SIGNAL(clicked()), this, SLOT(reverse()));
}

void UnaryOpWidget::shiftLeft()
{
    BitArray arr;
    if(chkBoxRot->isChecked())
        arr = (*array).rotateLeft(sBShiftVal->value());
    else
        arr = (*array) << sBShiftVal->value();

    emit operationExec(arr);
}

void UnaryOpWidget::shiftRight()
{
    BitArray arr;
    if(chkBoxRot->isChecked())
        arr = (*array).rotateRight(sBShiftVal->value());
    else
        arr= (*array) >> sBShiftVal->value();
    emit operationExec(arr);
}

void UnaryOpWidget::negate()
{
    BitArray arr = (BitArray)(~(*array));
    emit operationExec(arr);
}

void UnaryOpWidget::swap()
{
    BitArray arr = (BitArray)(array->byteSwap());
    emit operationExec(arr);
}

void UnaryOpWidget::reverse()
{
    BitArray arr = (BitArray)(array->bitReverse());
    emit operationExec(arr);
}


