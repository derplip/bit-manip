#ifndef BinaryOpWidget_H
#define BinaryOpWidget_H

#include <QWidget>
#include <QPushButton>
#include <QLineEdit>
#include <QBoxLayout>
#include <QLabel>
#include <QSpinBox>
#include <QRadioButton>
#include <QGroupBox>
#include "BitArray.h"

class BinaryOpWidget : public QWidget
{
     Q_OBJECT
public:
    BinaryOpWidget(QWidget *parent, BitArray *arr0, BitArray *arr1);

    void setArrays(BitArray *arr0, BitArray *arr1){array0 = arr0; array1 = arr1;}

    BitArray operate();

private:
    BitArray *array0, *array1;

    QPushButton *pBtnOr;
    QPushButton *pBtnAnd;
    QPushButton *pBtnNor;
    QPushButton *pBtnNand;
    QPushButton *pBtnXor;
    QPushButton *pBtnPlus;
    QPushButton *pBtnMinus;
    QPushButton *pBtnMult;
    QPushButton *pBtnDiv;

    int currOp;

private slots:
    void opOr();
    void opAnd();
    void opNor();
    void opNand();
    void opXor();
    void opPlus();
    void opMinus();
    void opMult();
    void opDiv();
    void updateButtons();

signals:
    void operationChanged();
};

#endif // BinaryOpWidget_H
