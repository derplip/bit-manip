#ifndef BITARRAY_H
#define BITARRAY_H

#include <QBitArray>
#include <QString>

class BitArray : public QBitArray
{
public:
    BitArray(){}
    BitArray(const QBitArray&);
    BitArray(int size, bool value = false);
    ~BitArray();

    void setResizeEnable(bool en){mResizeEnable = en;}
    bool isResizeEnabled(){return mResizeEnable;}
    bool resize(int size);
    void setSigned(bool sgd){isUnsigned = !sgd;}
    void setBEndian(bool be){lEndian = !be;}

    bool isSigned(){return !isUnsigned;}

    BitArray byteSwap();
    BitArray bitReverse();

    BitArray rotateLeft(int rotValue);
    BitArray rotateRight(int rotValue);

    BitArray operator+(const BitArray &other);
    BitArray operator-(const BitArray &other);
    BitArray operator*(const BitArray &other);
    BitArray operator/(const BitArray &other);
    BitArray operator<<(int shiftValue);
    BitArray operator>>(int shiftValue);

    bool operator>(const BitArray &other) const;
    bool operator<(const BitArray &other) const;
    bool operator>=(const BitArray &other) const;
    bool operator<=(const BitArray &other) const;
    bool operator==(const BitArray &other) const;

    bool isZero() const;

    QString toBinString(bool sep = false);
    QString toHexString(bool sep = false);
    QString toDecString();
    QString toAsciiString();

    bool fromHexString(QString);
    bool fromBinString(QString);
    bool fromDecString(QString decStr);
    bool fromAsciiString(QString asciiStr);


    void setValid(bool v){valid = v;}
    bool isValid(){return valid;}

private:
    bool lEndian;
    bool isUnsigned;
    bool valid;
    bool mResizeEnable;
};


#endif // BITARRAY_H
