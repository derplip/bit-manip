#ifndef OperationWidget_H
#define OperationWidget_H

#include <QWidget>
#include <QGroupBox>
#include <QLabel>
#include <QBoxLayout>
#include <QGridLayout>
#include <QCheckBox>
#include "OperandDisplay.h"
#include "BinaryOpWidget.h"

class OperationWidget : public QWidget
{
     Q_OBJECT
public:
    OperationWidget(QWidget * parent = 0);

private slots:
    void valueChangedSlot();

    void viewSettingsChanged();

private:
     OperandDisplay *resultDisplay;
     BinaryOpWidget* opWidget;

     QCheckBox *mCBoxEnAsciiView;
     QCheckBox *mCBoxEnBitView;
};

#endif // OperationWidget_H
