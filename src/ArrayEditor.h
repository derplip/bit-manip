#ifndef ArrayEditor_H
#define ArrayEditor_H

#include <QLineEdit>
#include <QSpinBox>
#include <QComboBox>
#include <QPalette>
#include <QLayout>
#include <QGroupBox>
#include <QCheckBox>

#include "BitArray.h"

class ArrayEditor : public QObject
{
    Q_OBJECT
public:
    explicit ArrayEditor(BitArray *array, bool editable, QObject *parent = 0);
    virtual QWidget *widget() = 0;
signals:
    void valueChanged();
public slots:
    virtual void updateView() = 0;
private slots:
protected:
    BitArray *mpArray;
    bool mEditable;
};

class ArraySizeEditor : public ArrayEditor
{
    Q_OBJECT
public:
    explicit ArraySizeEditor(BitArray *array, bool editable, QObject *parent = 0);
    QWidget *widget(){return mGroupBox;}
signals:
public slots:
    void updateView();
private slots:
    void sizeValChanged(int);
    void sizeMultChanged(int);
    void chkBoxSizeFixClicked();
protected:
    QGroupBox *mGroupBox;
    QSpinBox *sizeSpinBox;
    QComboBox *cBoxSizeMult;
    QCheckBox *chkBoxSizeFix;
};

class ArrayLineEditor : public ArrayEditor
{
    Q_OBJECT
public:
    explicit ArrayLineEditor(BitArray *array, bool editable, QObject *parent = 0);
    QWidget *widget(){return mpLineEdit;}
signals:
public slots:
    void updateView();
    virtual void updateCustomView() = 0;
private slots:
    virtual void textEditedSlot(QString text) = 0;
protected:
    void setEntryState(int state);
    QLineEdit *mpLineEdit;
    QPalette mPaletteNormal;
};

class ArrayHexEditor : public ArrayLineEditor
{
    Q_OBJECT
public:
    explicit ArrayHexEditor(BitArray *array, bool editable, QWidget *parent = 0);
signals:
public slots:
    virtual void updateCustomView();
private slots:
    void textEditedSlot(QString text);
private:
};

class ArrayBinEditor : public ArrayLineEditor
{
    Q_OBJECT
public:
    explicit ArrayBinEditor(BitArray *array, bool editable, QWidget *parent = 0);
signals:
public slots:
    virtual void updateCustomView();
private slots:
    void textEditedSlot(QString text);
private:
};

class ArrayDecEditor : public ArrayLineEditor
{
    Q_OBJECT
public:
    explicit ArrayDecEditor(BitArray *array, bool editable, QWidget *parent = 0);
signals:
public slots:
    virtual void updateCustomView();
private slots:
    void textEditedSlot(QString text);
private:
};

class ArrayAsciiEditor : public ArrayLineEditor
{
    Q_OBJECT
public:
    explicit ArrayAsciiEditor(BitArray *array, bool editable, QWidget *parent = 0);
signals:
public slots:
    virtual void updateCustomView();
private slots:
    void textEditedSlot(QString text);
private:
};

#endif // ArrayEditor_H
