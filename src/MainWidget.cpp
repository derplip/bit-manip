#include "MainWidget.h"



MainWidget::MainWidget(QWidget *parent) :
    QWidget(parent)
{
    QVBoxLayout *topLayout = new QVBoxLayout(this);
    layout = new QVBoxLayout();
    //scrollArea = new QScrollArea(this);
    desktopWidget = new QDesktopWidget();
    operationWidget = new OperationWidget(this);

    layout->addWidget(operationWidget);
    layout->setContentsMargins(0, 0, 0, 0);
    topLayout->addLayout(layout);
    topLayout->insertStretch(-1);
    topLayout->setContentsMargins(0, 0, 0, 0);

    this->setWindowIcon(QIcon(":/icons/binary2.png"));

    this->setWindowTitle("BitManip v1.3");

    //rthis->resize(600, 400);
}

