#ifndef UnaryOpWidget_H
#define UnaryOpWidget_H

#include <QWidget>
#include <QPushButton>
#include <QLineEdit>
#include <QBoxLayout>
#include <QLabel>
#include <QSpinBox>
#include <QGroupBox>
#include <QCheckBox>
#include <QRadioButton>
#include "BitArray.h"

class UnaryOpWidget : public QWidget
{
    Q_OBJECT
public:
    UnaryOpWidget(BitArray *arr, QWidget *parent = 0);

    void setArray(BitArray *arr){array = arr;}
    QWidget *widget(){return mGroupBox;}

private:
    BitArray *array;

    QGroupBox *mGroupBox;

    QPushButton *pBtnShiftRight;
    QPushButton *pBtnShiftLeft;
    QPushButton *pBtnNegate;
    QPushButton *pBtnSwap;
    QPushButton *pBtnReverse;
    QCheckBox *chkBoxRot;

    QSpinBox *sBShiftVal;

private slots:
    void shiftLeft();
    void shiftRight();
    void negate();
    void swap();
    void reverse();

signals:
    void operationExec(BitArray);
};

#endif // UnaryOpWidget_H
