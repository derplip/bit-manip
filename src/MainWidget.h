#ifndef MAINWIDGET_H
#define MAINWIDGET_H

#include <QWidget>
#include <QDesktopWidget>
#include <QBoxLayout>
#include <QGroupBox>
#include <QScrollArea>

#include "OperationWidget.h"
#include "BinaryOpWidget.h"

class MainWidget : public QWidget
{
    Q_OBJECT
public:
    explicit MainWidget(QWidget *parent = 0);
    
signals:
    
public slots:

protected:

private:
    QVBoxLayout *layout;
    QScrollArea *scrollArea;
    OperationWidget *operationWidget;
    QDesktopWidget *desktopWidget;
};

#endif // MAINWIDGET_H
