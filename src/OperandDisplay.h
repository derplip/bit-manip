#ifndef OperandDisplay_H
#define OperandDisplay_H

#include <QWidget>
#include <QTableWidget>
#include <QLineEdit>
#include <QBoxLayout>
#include <QLabel>
#include <QSpinBox>
#include <QRadioButton>
#include <QCheckBox>
#include <QComboBox>

#include "BitArray.h"
#include "UnaryOpWidget.h"
#include "BitEditor.h"
#include "ArrayEditor.h"

class OperandDisplay : public QWidget
{
    Q_OBJECT
public:
    explicit OperandDisplay(QWidget *parent = 0, bool editable = true);
    BitArray *getArray(){return array;}

signals:

    void valueChanged();

public slots:

    void updateView(int updateMask = 0);

private slots:
    void binValChanged();
    void hexValChanged();
    void dezValChanged();
    void ascValChanged();
    void cBoxValChanged();
    void sizeValChanged();
    void signChanged();
    void endianChanged();
    void operationExecSlot(BitArray);
    void showHideCBoxDisplay();

private:
    BitArray *array;

    ArrayEditor *binDisplay;
    ArrayEditor *hexDisplay;
    ArrayEditor *dezDisplay;
    ArrayEditor *ascDisplay;

    ArrayEditor *sizeEditor;

    BitEditor *mCBoxDisplay;
    QPushButton *mPbShowCBoxDisplay;

//    QRadioButton *rBtnUnsigned;
//    QRadioButton *rBtnSigned;
//    QRadioButton *rBtnLEndian;
//    QRadioButton *rBtnBEndian;


        QCheckBox *cBoxSigned;
        QCheckBox *cBoxBEndian;

    QCheckBox *chkBoxShowSep;

    UnaryOpWidget *opWidget;

    bool mEditable;
};

#endif // OperandDisplay_H
