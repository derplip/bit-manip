# -------------------------------------------------
# Project created by QtCreator 2011-07-12T21:01:22
# -------------------------------------------------
QT += core \
    gui widgets
TARGET = BitManip
TEMPLATE = app
SOURCES += src/main.cpp \
    src/BitArray.cpp \
    src/OperandDisplay.cpp \
    src/BitEditor.cpp \
    src/MainWidget.cpp \
    src/OperationWidget.cpp \
    src/BinaryOpWidget.cpp \
    src/UnaryOpWidget.cpp \
    src/ArrayEditor.cpp
HEADERS += BitArray.h \
    src/OperandDisplay.h \
    src/BitEditor.h \
    src/BitArray.h \
    src/MainWidget.h \
    src/OperationWidget.h \
    src/BinaryOpWidget.h \
    src/UnaryOpWidget.h \
    src/ArrayEditor.h \
    src/global.h \
    src/global.h

RESOURCES += \
    ressource.qrc
